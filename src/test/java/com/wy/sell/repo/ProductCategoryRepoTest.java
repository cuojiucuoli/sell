package com.wy.sell.repo;

import com.wy.sell.entity.ProductCategory;
import org.hibernate.criterion.Example;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryRepoTest {

    @Autowired
    ProductCategoryRepo productCategoryRepo;

    @Test
    public void findOneTest(){

        List<ProductCategory> all = productCategoryRepo.findAll();
        System.out.println(all);
    }

    @Test
    @Transactional
    public void saveOneTest(){
        ProductCategory productCategory = new ProductCategory();
        productCategory.setCategoryName("豆腐2");
        productCategory.setCategoryType(2);
        productCategoryRepo.save(productCategory);
    }


    @Test
    public void findCategoryType(){
        List<Integer> alist = Arrays.asList(1, 2, 3);
        List<ProductCategory> categoryTypeIn = productCategoryRepo.findByCategoryTypeIn(alist);
        Assert.assertNotEquals(0,categoryTypeIn.size());
    }
}