package com.wy.sell.repo;

import com.wy.sell.entity.ProductInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OrderRepoTest {

    @Autowired
    OrderDetailRepo orderRepo;

    @Autowired
    ProductRepo productRepo;

    @Test
    public void findProductInfoByProductIdIn() {
        List<String> list = Arrays.asList("-4889516205425667402", "2");
        List<ProductInfo> productInfoByProductIdIn = productRepo.findProductInfoByProductStatusAndProductIdIn(0,list);
        System.out.println(productInfoByProductIdIn);
    }
}