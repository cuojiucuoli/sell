package com.wy.sell.repo;

import com.wy.sell.entity.ProductInfo;
import com.wy.sell.enums.ProductStatusEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepoTest {
    @Autowired
    ProductRepo productRepo;


    @Test
    public void saveProductInfo(){
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId(String.valueOf(UUID.randomUUID().getLeastSignificantBits()));
        productInfo.setCategoryType(1);
        productInfo.setProductDescription("豆腐是用来吃的");
        productInfo.setProductIcon("www.baidu.com");
        productInfo.setProductStock(ProductStatusEnum.UP.getStatusCode());
        productInfo.setProductPrice(new BigDecimal(3.3));
        productInfo.setProductName("红烧豆腐");
        productRepo.save(productInfo);
    }
}