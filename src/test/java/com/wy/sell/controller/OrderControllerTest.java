package com.wy.sell.controller;

import com.wy.sell.entity.OrderMaster;
import com.wy.sell.repo.OrderMasterRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
@SpringBootTest
@RunWith(SpringRunner.class)
public class OrderControllerTest {
    @Autowired
    OrderMasterRepo orderMasterRepo;
    @Test
    public void findOrderList() {
        List<Integer> list = Arrays.asList(1, 0);
        Pageable pageable = new PageRequest(0, 1);
        Page<OrderMaster> pa = orderMasterRepo.findOrderMasterByBuyerOpenidAndOrderStatusIn("ew3euwhd7sjw9diwkq", list, pageable);
        List<OrderMaster> content = pa.getContent();
        System.out.println(content);
    }
}