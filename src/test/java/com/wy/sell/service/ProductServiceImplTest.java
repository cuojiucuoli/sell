package com.wy.sell.service;

import com.wy.sell.entity.ProductInfo;
import com.wy.sell.page.PageDTO;
import com.wy.sell.page.PageVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceImplTest {

    @Autowired
    ProductServiceImpl productService;

    @Test
    public void findProductByPage(){
        PageDTO<ProductInfo> pageDTO = new PageDTO<>();
        pageDTO.setPageSize(4);
        pageDTO.setPageNum(0);
        PageVO<List<ProductInfo>> page = productService.findProductByPage(pageDTO);
        List<ProductInfo> t = page.getT();
        Assert.assertNotEquals(0,t.size());
    }

    @Test
    public void findProductByPageAndCType(){
        PageDTO<ProductInfo> pageDTO = new PageDTO<>();
        pageDTO.setPageNum(0);
        pageDTO.setPageSize(5);
        ProductInfo productInfo = new ProductInfo();
        productInfo.setCategoryType(1);
        pageDTO.setT(productInfo);
        PageVO<List<ProductInfo>> productByPageAndCategoryType = productService.findProductByPageAndCategoryType(pageDTO);
        System.out.println(productByPageAndCategoryType.getT());
    }

}