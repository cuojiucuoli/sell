package com.wy.sell.controller;

import com.wy.sell.entity.OrderMaster;
import com.wy.sell.service.OrderServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class BackOrderController {

    @Autowired
    OrderServiceImp orderServiceImp;

    @RequestMapping("/order/list")
    public ModelAndView getOrderList(@RequestParam(value = "page",defaultValue = "1") Integer page, @RequestParam(value = "size",defaultValue = "10") Integer size, String buyerPhone, String orderId){
        Page<OrderMaster> pages = orderServiceImp.backOrderList(page, size, buyerPhone, orderId);
        Map<String,Object> map = new HashMap<>();
        map.put("orderDTOPage",pages);
        map.put("currentPage",page);
        ModelAndView modelAndView = new ModelAndView("order/list",map);

        return modelAndView;
    }
}
