package com.wy.sell.controller;

import com.wy.sell.dto.OrderDTO;
import com.wy.sell.entity.OrderMaster;
import com.wy.sell.enums.ResultEnum;
import com.wy.sell.exception.SellException;
import com.wy.sell.request.CreateOrderRequest;
import com.wy.sell.response.DataResponse;
import com.wy.sell.service.OrderServiceImp;
import com.wy.sell.util.OrderJsonTransfer;
import com.wy.sell.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.xml.ws.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/buyer/order")
public class OrderController {

    @Autowired
    OrderServiceImp orderServiceImp;
    @RequestMapping("/create")
    public DataResponse createOrder(@Valid CreateOrderRequest orderRequest, BindingResult b) {
        if (b.hasErrors()) {
            throw new SellException(ResultEnum.PARAM_ERROR.getCode(), b.getFieldError().getDefaultMessage());
        }
        OrderDTO orderDTO = OrderJsonTransfer.transferCreateOrderJsonToOrderDTO(orderRequest);
        String orderId = orderServiceImp.createOrder(orderDTO).getOrderId();
        Map<String,Object> map = new HashMap<>();
        map.put("orderId",orderId);
        return ResponseUtil.successResponse(map);
    }


    @RequestMapping("/detail")
    public DataResponse orderDetail(@Valid @NotEmpty(message = "openId不能为空") String openid,@Valid @NotEmpty(message = "orderId不能为空")  String orderId) {
       return ResponseUtil.successResponse(orderServiceImp.findOrderInfo(openid,orderId));
    }

    @RequestMapping("/list")
    public DataResponse findOrderList(String openid, Integer page, Integer size){
        List<OrderMaster> orderList = orderServiceImp.findOrderList(openid, page, size);
        return ResponseUtil.successResponse(orderList);
    }


    @RequestMapping("/cancel")
    public DataResponse cancelOrder(String openid,String orderId){
        orderServiceImp.cancelOrder(openid,orderId);
        return ResponseUtil.successResponse(null);
    }


    /**
     * 后台管理系统
     */


}
