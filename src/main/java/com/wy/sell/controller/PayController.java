package com.wy.sell.controller;

import com.alibaba.fastjson.JSONObject;
import com.wy.sell.config.PayJSConfig;
import com.wy.sell.dto.OrderDTO;
import com.wy.sell.enums.OrderStatusEnum;
import com.wy.sell.enums.PayStatusEnum;
import com.wy.sell.enums.ResultEnum;
import com.wy.sell.exception.SellException;
import com.wy.sell.response.NotifyResponse;
import com.wy.sell.service.OrderServiceImp;
import com.wy.sell.service.PayServiceImpl;
import com.wy.sell.util.HttpInvoker;
import com.wy.sell.util.SignUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.wy.sell.enums.ResultEnum.WXPAY_NOTIFY_MONEY_VERIFY_ERROR;

@Controller
@RequestMapping("/pay")
public class PayController {

    @Autowired
    OrderServiceImp orderServiceImp;

    @Autowired
    PayServiceImpl payService;

    @Autowired
    private PayJSConfig payJSConfig;

    @GetMapping(value = "create")
    public ModelAndView create(@RequestParam("orderId")String orderId,@RequestParam("returnUrl")String returnUrl){
            //查询订单
        OrderDTO orderDTO = orderServiceImp.findOrderById(orderId);
        if(orderDTO==null||orderDTO.getPayStatus()!= PayStatusEnum.WAIT.getCode()||orderDTO.getOrderStatus()!=OrderStatusEnum.NEW.getCode()){
            throw new SellException(ResultEnum.ORDER_NOT_EXIST);
        }

        if(orderDTO.getOrderAmount()==null){
            orderDTO.setOrderAmount(new BigDecimal(0));
        }
        Map<String, String> map = payService.payOrder(orderDTO,returnUrl);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("pay");
        modelAndView.addAllObjects(map);
        return modelAndView;
    }


    /*@RequestMapping(value = "/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/pay")
    public ModelAndView pay(BigDecimal totalFee, String body, Integer type, RedirectAttributes redirectAttributes){
        Integer total_fee = (totalFee.multiply(new BigDecimal(100)).intValue());
        redirectAttributes.addAttribute("money", total_fee);
        redirectAttributes.addAttribute("body", body);
        if(type == 1){
            return new ModelAndView("redirect:/nativePay");
        } else if(type == 2){
            return new ModelAndView("redirect:/cashierPay");
        }
        return null;
    }*/

    /**
     * 扫码支付
     * @param money
     * @return
     */
    /*@RequestMapping(value = "nativePay")
    public ModelAndView nativePay(Integer money, String body){
        ModelAndView mv = new ModelAndView();
        Map<String,String> map = new HashMap<>();
        map.put("mchid", payJSConfig.getMchid());
        map.put("total_fee",""+money);
        String out_trade_no = "order"+System.currentTimeMillis();
        map.put("out_trade_no",out_trade_no);
        map.put("body",body);
        map.put("notify_url", "https://payjs.cn/help/");//请注意，，该路径需要payjs服务器可以直接访问，且结果为200。测试地址不行，www.baidu.com也不行
        String md5 = SignUtil.sign(map, payJSConfig.getKey());
        map.put("sign", md5.toUpperCase());
        map.put("body", map.get("body"));
        map.put("notify_url", map.get("notify_url"));
        String result = HttpInvoker.readContentFromPost("https://payjs.cn/api/native", map);
        JSONObject jsonObject = JSONObject.parseObject(result);
        if(null != jsonObject && jsonObject.containsKey("qrcode")){
            mv.addObject("qrcode", jsonObject.getString("qrcode"));
            mv.setViewName("result");
        } else if(null != jsonObject && jsonObject.containsKey("msg")){
            mv.addObject("reason", UnicodeUtil.decodeUnicode(jsonObject.getString("msg")));
            mv.setViewName("error");
        }
        return mv;
    }*/

    /**
     * 收银台支付
     * @param
     * @param
     * @return
     */
    /*@RequestMapping(value = "/cashierPay")
    public ModelAndView cashierPay(Integer money, String body) {
        ModelAndView mv = new ModelAndView("pay");

        String mchid = payJSConfig.getMchid();
        String out_trade_no = "order"+System.currentTimeMillis();
        String notify_url = "https://payjs.cn/help/";//请注意，，该路径需要payjs服务器可以直接访问，且http状态码为200。测试地址不行，www.baidu.com也不行
        String callback_url = "https://payjs.cn/help/";

        Map<String,String> map = new HashMap<>();
        map.put("mchid", mchid);
        map.put("total_fee",""+money);
        map.put("out_trade_no",out_trade_no);
        map.put("body",body);
        map.put("notify_url", notify_url);
        map.put("callback_url", callback_url);
        String md5 = SignUtil.sign(map, payJSConfig.getKey());

        String sign = md5.toUpperCase();

        mv.addObject("mchid", mchid);
        mv.addObject("total_fee", money);
        mv.addObject("out_trade_no", out_trade_no);
        mv.addObject("body", body);
        mv.addObject("notify_url", notify_url);
        mv.addObject("callback_url", callback_url);
        mv.addObject("sign", sign);
        return mv;
    }*/


  /*  @ResponseBody
    @RequestMapping(value = "/notify/{outOrderNumber}")
    public Map<String,Object> payNotify(@PathVariable String outOrderNumber, String return_code, String total_fee,
                                        String out_trade_no, String payjs_order_id, String transaction_id, String time_end,
                                        String openid, String mchid, String sign){
        System.out.println("收到通知-----");
        System.out.println("outOrderNumber:"+outOrderNumber);
        System.out.println("return_code:"+return_code);
        System.out.println("total_fee:"+total_fee);
        System.out.println("out_trade_no:"+out_trade_no);
        System.out.println("payjs_order_id:"+payjs_order_id);
        System.out.println("transaction_id:"+transaction_id);
        System.out.println("time_end:"+time_end);
        System.out.println("openid:"+openid);
        System.out.println("mchid:"+mchid);
        System.out.println("sign:"+sign);
        return null;
    }*/


    @ResponseBody
    @RequestMapping(value = "/notify/{outOrderNumber}")
    public void payNotify(@PathVariable String outOrderNumber,NotifyResponse notifyResponse){


        if(StringUtils.isBlank(notifyResponse.getReturn_code())||StringUtils.isBlank(notifyResponse.getPayjs_order_id())){
            throw new SellException(WXPAY_NOTIFY_MONEY_VERIFY_ERROR);
        }


        OrderDTO orderDTO = orderServiceImp.findOrderById(outOrderNumber);

        String sign = notifyResponse.getSign();
        notifyResponse.setSign(null);
        String old_sign = SignUtil.getSign(notifyResponse, payJSConfig.getKey());
        if(StringUtils.isBlank(old_sign)||!old_sign.equals(sign)){
            throw new SellException(WXPAY_NOTIFY_MONEY_VERIFY_ERROR);
        }

        double tranferFee = Double.valueOf(notifyResponse.getTotal_fee()) / 100;
        double dbFee = orderDTO.getOrderAmount().doubleValue();
        if(dbFee!=tranferFee){
            throw new SellException(WXPAY_NOTIFY_MONEY_VERIFY_ERROR);
        }
        Integer status = orderServiceImp.updateOrderMaster(outOrderNumber,notifyResponse.getPayjs_order_id());
        if(status==-1){
            return;
        }
        Map<String,String> map = new HashMap<>();

        map.put("payjs_order_id",notifyResponse.getPayjs_order_id());
        String requestSign = SignUtil.sign(map, payJSConfig.getKey());
        String s1 = requestSign.toUpperCase();
        map.put("sign",s1);
        String s = HttpInvoker.readContentFromPost("https://payjs.cn/api/check", map);


    }



}
