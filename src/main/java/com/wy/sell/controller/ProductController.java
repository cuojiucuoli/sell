package com.wy.sell.controller;


import com.wy.sell.entity.ProductCategory;
import com.wy.sell.entity.ProductInfo;
import com.wy.sell.response.DataResponse;
import com.wy.sell.service.ProductServiceImpl;
import com.wy.sell.util.ResponseUtil;
import com.wy.sell.vo.ProductInfoVO;
import com.wy.sell.vo.ProductListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/buyer/product")
public class ProductController {

    @Autowired
    ProductServiceImpl productService;

    @RequestMapping("/list")
    public DataResponse<List<ProductListVO>> produictList() {

        List<Map<String, Object>> list = productService.showProductList();
        List<ProductListVO> productCategoryList = new ArrayList<>();
        if (list != null && !list.isEmpty()) {
            List<Integer> categoryType = list.stream().map(e -> (Integer) e.get("category_type")).distinct().collect(Collectors.toList());

            for (Integer type : categoryType) {
                ProductListVO productListVO = new ProductListVO();
                productCategoryList.add(productListVO);
                productListVO.setCategoryType(type);

                List<ProductInfoVO> productInfoVOList = new ArrayList<>();

                List<Map<String, Object>> productList = list.stream().filter(map -> type == (Integer) map.get("category_type")).collect(Collectors.toList());

                productList.forEach(map -> {
                    ProductInfoVO productInfoVO = new ProductInfoVO((String) map.get("product_id"), (String) map.get("product_name"), (BigDecimal) map.get("product_price"), (String) map.get("product_description"), (String) map.get("product_icon"));
                    productInfoVOList.add(productInfoVO);
                    productListVO.setCategoryName((String) map.get("category_name"));
                });
                productListVO.setProductInfo(productInfoVOList);


            }
        }

        return ResponseUtil.successResponse(productCategoryList);
    }
}

