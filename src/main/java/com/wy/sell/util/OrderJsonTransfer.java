package com.wy.sell.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wy.sell.dto.OrderDTO;
import com.wy.sell.entity.OrderDetail;
import com.wy.sell.request.CreateOrderRequest;

import java.util.ArrayList;
import java.util.List;

public class OrderJsonTransfer {

    public static OrderDTO transferCreateOrderJsonToOrderDTO(CreateOrderRequest createOrderRequest){
        OrderDTO orderDTO= new OrderDTO();
        orderDTO.setBuyerName(createOrderRequest.getName());
        orderDTO.setBuyerAddress(createOrderRequest.getAddress());
        orderDTO.setBuyerPhone(createOrderRequest.getPhone());
        orderDTO.setBuyerOpenid(createOrderRequest.getOpenid());
        String items = createOrderRequest.getItems();
        List<OrderDetail> orderDetailList = new ArrayList<>();
        Gson gson = new Gson();
        orderDetailList = gson.fromJson(items, new TypeToken<List<OrderDetail>>() {
        }.getType());
        orderDTO.setOrderDetailList(orderDetailList);
        return orderDTO;
    }
}
