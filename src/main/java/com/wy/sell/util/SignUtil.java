package com.wy.sell.util;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.serializer.SerializeConfig;
import org.springframework.util.DigestUtils;

import java.util.*;

public class SignUtil {

    public static String sign(Map<String,String> map, String privateKey){
        Collection<String> keyset= map.keySet();
        List<String> keyList= new ArrayList<>(keyset);
        Collections.sort(keyList);
        StringBuilder sb = new StringBuilder();
        for (String key : keyList){
            sb.append(key).append("=").append(map.get(key)).append("&");
        }
        sb.append("key=").append(privateKey);
        return DigestUtils.md5DigestAsHex(sb.toString().getBytes());
    }


    public static String getSign(Object object, String key) {

        SerializeConfig config = new SerializeConfig();
        config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;
        String jsonString = JSONObject.toJSONString(object, config);
        JSONObject jsonObject = JSONObject.parseObject(jsonString);

        // 排序
        Set<String> keys = jsonObject.keySet();
        List<String> keysList = new ArrayList<>(keys.size());
        keys.forEach(item -> keysList.add(item));
        Collections.sort(keysList);

        StringBuilder stringBuilder = new StringBuilder();
        keysList.forEach(item -> {
            stringBuilder.append(item + "=" + jsonObject.get(item) + "&");
        });
        stringBuilder.append("key=" + key);

        return DigestUtils.md5DigestAsHex(stringBuilder.toString().getBytes()).toUpperCase();
    }


}
