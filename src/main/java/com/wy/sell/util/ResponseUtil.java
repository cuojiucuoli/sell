package com.wy.sell.util;

import com.wy.sell.response.DataResponse;

public class ResponseUtil {


    public static DataResponse successResponse(Object o){
        DataResponse dataResponse = new DataResponse();
        dataResponse.setCode(0);
        dataResponse.setMsg("成功");
        dataResponse.setData(o);
        return  dataResponse;
    }
}
