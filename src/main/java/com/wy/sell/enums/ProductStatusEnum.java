package com.wy.sell.enums;

public enum ProductStatusEnum {

    UP(0, "上架"),
    DOWN(0, "下架");
    private Integer statusCode;
    private String message;

    ProductStatusEnum(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    ;

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
