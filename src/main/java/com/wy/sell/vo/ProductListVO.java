package com.wy.sell.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wy.sell.entity.ProductInfo;

import java.util.List;

public class ProductListVO {
    @JsonProperty("name")
    private String categoryName;
    @JsonProperty("type")
    private Integer categoryType;
    @JsonProperty("foods")
    private List<ProductInfoVO> productInfo;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Integer categoryType) {
        this.categoryType = categoryType;
    }

    public List<ProductInfoVO> getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(List<ProductInfoVO> productInfo) {
        this.productInfo = productInfo;
    }
}
