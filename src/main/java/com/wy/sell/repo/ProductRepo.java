package com.wy.sell.repo;

import com.wy.sell.entity.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface  ProductRepo extends JpaRepository<ProductInfo,String> {
    List<ProductInfo> findProductInfoByProductStatusAndProductIdIn(Integer status, List<String> productId);
}
