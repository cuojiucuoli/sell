package com.wy.sell.repo;

import com.wy.sell.entity.OrderDetail;
import com.wy.sell.entity.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

public interface OrderDetailRepo extends JpaRepository<OrderDetail,String>{


    List<OrderDetail> findOrderDetailByOrderId(String orderId);

}
