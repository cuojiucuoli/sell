package com.wy.sell.repo;

import com.wy.sell.dto.OrderDTO;
import com.wy.sell.entity.OrderDetail;
import com.wy.sell.entity.OrderMaster;
import com.wy.sell.entity.ProductInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

public interface OrderMasterRepo extends JpaRepository<OrderMaster,String>{

    Page<OrderMaster> findOrderMasterByBuyerOpenidAndOrderStatusIn(String openid, List<Integer> list, Pageable pageable);

    List<OrderMaster> findOrderMasterByBuyerOpenidAndOrderIdAndOrderStatusNot(String openid, String orderId,Integer status);

    List<OrderMaster> findOrderMasterByBuyerOpenidAndOrderId(String openid, String orderId);
}
