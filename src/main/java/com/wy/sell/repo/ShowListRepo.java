package com.wy.sell.repo;

import com.wy.sell.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface ShowListRepo extends JpaRepository<ProductCategory,Integer>{
    @Query(value = "select product_category.category_name," +
            "product_category.category_type, " +
            "product_name," +
            "product_id," +
            "product_price," +
            "product_description," +
            "product_icon  " +
            "from product_category " +
            "left join product_info " +
            "on product_category.category_type = product_info.category_type " +
            "and " +
            "product_info.product_status = 0",nativeQuery = true)
    public List<Map<String,Object>> productShowList();
}
