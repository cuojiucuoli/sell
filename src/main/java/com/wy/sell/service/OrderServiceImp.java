package com.wy.sell.service;

import com.wy.sell.config.PayJSConfig;
import com.wy.sell.dto.OrderDTO;
import com.wy.sell.entity.OrderDetail;
import com.wy.sell.entity.OrderMaster;
import com.wy.sell.entity.ProductInfo;
import com.wy.sell.enums.OrderStatusEnum;
import com.wy.sell.enums.PayStatusEnum;
import com.wy.sell.enums.ProductStatusEnum;
import com.wy.sell.enums.ResultEnum;
import com.wy.sell.exception.SellException;
import com.wy.sell.repo.OrderDetailRepo;
import com.wy.sell.repo.OrderMasterRepo;
import com.wy.sell.repo.ProductRepo;
import com.wy.sell.util.HttpInvoker;
import com.wy.sell.util.SignUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.sun.webkit.CursorManager.WAIT;
import static com.wy.sell.enums.OrderStatusEnum.CANCEL;
import static com.wy.sell.enums.OrderStatusEnum.NEW;
import static com.wy.sell.enums.ResultEnum.ORDERDETAIL_NOT_EXIST;
import static com.wy.sell.enums.ResultEnum.ORDER_STATUS_ERROR;
import static com.wy.sell.enums.ResultEnum.WXPAY_NOTIFY_MONEY_VERIFY_ERROR;

@Service
public class OrderServiceImp {

    @Autowired
    OrderDetailRepo orderDetailRepo;

    @Autowired
    OrderMasterRepo orderMasterRepo;

    @Autowired
    ProductRepo  productRepo;

    @Autowired
    PayJSConfig payJSConfig;

    @Transactional
    public OrderMaster createOrder(OrderDTO orderDTO)  {
        List<OrderDetail> orderDetailList = orderDTO.getOrderDetailList();
        //查询创建的订单商品是否上架

        List<String> orderIds = orderDetailList.stream().map(orderDetail -> orderDetail.getProductId()).distinct().collect(Collectors.toList());
        List<ProductInfo> list = productRepo.findProductInfoByProductStatusAndProductIdIn(ProductStatusEnum.UP.getStatusCode(), orderIds);
        if (list == null || orderIds.size() != list.size()) {
            throw new SellException(ResultEnum.PRODUCT_NOT_EXIST);
        }
        
        
        //查询订单总价
        OrderMaster orderMaster = new OrderMaster();
        BeanUtils.copyProperties(orderDTO, orderMaster);
        String orderId = UUID.randomUUID().getLeastSignificantBits()+"";
        orderMaster.setOrderId(orderId);

        BigDecimal totalMoney = new BigDecimal(0);
        for (OrderDetail orderDetail : orderDetailList) {
            ProductInfo productInfo = productRepo.findById(orderDetail.getProductId()).get();
            BigDecimal multiply = productInfo.getProductPrice().multiply(new BigDecimal(orderDetail.getProductQuantity()));
            totalMoney = totalMoney.add(multiply);
            BeanUtils.copyProperties(productInfo,orderDetail);
            orderDetail.setOrderId(orderId);
            orderDetail.setDetailId(UUID.randomUUID().getLeastSignificantBits()+"");
        }
        orderMaster.setOrderAmount(totalMoney);
        orderMaster.setOrderStatus(OrderStatusEnum.NEW.getCode());
        orderMaster.setPayStatus(PayStatusEnum.WAIT.getCode());
        //保存订单和订单详情
        try {

            orderMasterRepo.save(orderMaster);
            orderDetailRepo.saveAll(orderDetailList);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SellException(ResultEnum.ORDER_UPDATE_FAIL.getCode(),ResultEnum.ORDER_UPDATE_FAIL.getMessage());
        }

        return orderMaster;
    }

    public OrderDTO findOrderInfo(@Valid @NotEmpty(message = "openId不能为空") String openid, @Valid @NotEmpty(message = "orderId不能为空") String orderId) {
        List<OrderMaster> orderMasters = orderMasterRepo.findOrderMasterByBuyerOpenidAndOrderIdAndOrderStatusNot(openid, orderId,CANCEL.getCode());
        
        if (orderMasters==null||orderMasters.size()==0){
            throw new SellException(ORDERDETAIL_NOT_EXIST);
        }

        OrderMaster orderMaster = orderMasters.get(0);
        List<OrderDetail> orderDetails = orderDetailRepo.findOrderDetailByOrderId(orderMaster.getOrderId());
        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(orderMaster,orderDTO);
        orderDTO.setOrderDetailList(orderDetails);
        return  orderDTO;
    }

    public List<OrderMaster> findOrderList( String openid, Integer page, Integer size) {
        List<Integer> list = Arrays.asList(1, 0);
        Pageable pageable = new PageRequest(page, size);
        Page<OrderMaster> pageData = orderMasterRepo.findOrderMasterByBuyerOpenidAndOrderStatusIn(openid, list, pageable);
        List<OrderMaster> content = pageData.getContent();
        return content;
    }

    @Transactional
    public void cancelOrder(String openid, String orderId) {
        List<OrderMaster> orderMasters = orderMasterRepo.findOrderMasterByBuyerOpenidAndOrderId(openid, orderId);
        if(orderMasters==null||orderMasters.size()==0){
            throw new SellException(ORDERDETAIL_NOT_EXIST);
        }
        OrderMaster orderMaster = orderMasters.get(0);
        Integer orderStatus = orderMaster.getOrderStatus();
        Integer payStatus = orderMaster.getPayStatus();
        if (orderStatus!=OrderStatusEnum.NEW.getCode()){
                throw new SellException(ORDER_STATUS_ERROR);
        }
        //如果订单还未付款，直接删除订单 无须商家确认，且真正物理删除。
        if(payStatus==WAIT){
            try {
                List<OrderDetail> orderDetailByOrderId = orderDetailRepo.findOrderDetailByOrderId(orderId);
                orderMasterRepo.delete(orderMaster);
                orderDetailRepo.deleteAll(orderDetailByOrderId);
            } catch (Exception e) {
                e.printStackTrace();
                throw new SellException(ORDER_STATUS_ERROR);
            }
        }else{
            //需要商家确认，并且退款，非物理删除
            HashMap<String,String> map = new HashMap<>();
            map.put("payjs_order_id",orderMaster.getPayJsOrderId());
            String sign = SignUtil.sign(map, payJSConfig.getKey());
            String s = sign.toUpperCase();
            map.put("sign",s);
            String returnStr = HttpInvoker.readContentFromPost("https://payjs.cn/api/check", map);
            System.out.println(returnStr);
            //TODO
        }


    }

    public OrderDTO findOrderById(String orderId){
        if(orderMasterRepo.findById(orderId).isPresent()){
            OrderMaster orderMaster = orderMasterRepo.findById(orderId).get();
            OrderDTO orderDTO = new OrderDTO();
            BeanUtils.copyProperties(orderMaster,orderDTO);
            return orderDTO;
        }
        throw new SellException(ResultEnum.ORDER_NOT_EXIST);

    }

    public Integer updateOrderMaster(String orderId,String payJsId){
        OrderMaster orderMaster = orderMasterRepo.getOne(orderId);
        Integer orderStatus = orderMaster.getOrderStatus();
        Integer payStatus = orderMaster.getPayStatus();
        if(NEW.getCode()!=orderStatus||PayStatusEnum.WAIT.getCode()!=payStatus){
            return -1;
        }
        orderMaster.setPayStatus(PayStatusEnum.SUCCESS.getCode());
        orderMaster.setPayJsOrderId(payJsId);
        OrderMaster save = orderMasterRepo.save(orderMaster);
        if(save==null){
            throw new SellException(WXPAY_NOTIFY_MONEY_VERIFY_ERROR);
        }
        return 0;
    }


    public Page<OrderMaster> backOrderList(Integer page, Integer size,String buyerPhone,String orderId){
        PageRequest pageable = PageRequest.of(page - 1, size);
        OrderMaster orderMaster = new OrderMaster();
        ExampleMatcher matcher = ExampleMatcher.matchingAny();
        if(StringUtils.isNotBlank(buyerPhone)){
            orderMaster.setBuyerPhone(buyerPhone);
            matcher = ExampleMatcher.matching().withMatcher("buyerPhone", ExampleMatcher.GenericPropertyMatchers.exact());
        }
        if(StringUtils.isNotBlank(orderId)){
            orderMaster.setOrderId(orderId);
            matcher.withMatcher("orderId", ExampleMatcher.GenericPropertyMatchers.exact());
        }


        Example<OrderMaster> of = Example.of(orderMaster, matcher);
        Page<OrderMaster> all = orderMasterRepo.findAll(of, pageable);
        return all;
    }
}
