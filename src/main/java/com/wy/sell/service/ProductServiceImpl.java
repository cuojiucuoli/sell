package com.wy.sell.service;

import com.wy.sell.entity.ProductInfo;
import com.wy.sell.page.PageDTO;
import com.wy.sell.page.PageVO;
import com.wy.sell.repo.ProductRepo;
import com.wy.sell.repo.ShowListRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl {
    @Autowired
    ProductRepo productRepo;

    @Autowired
    ShowListRepo showListRepo;

    public ProductInfo findProductInfoById(String productId) throws Exception {
        if (productRepo.findById(productId).isPresent()) {
            return productRepo.findById(productId).get();
        } else {
            throw new Exception();
        }
    }


    public PageVO<List<ProductInfo>> findProductByPage(PageDTO<ProductInfo> page) {
        PageRequest pageRequest = PageRequest.of(page.getPageNum(), page.getPageSize());
        PageVO<List<ProductInfo>> pageVO = new PageVO<>();
        Page<ProductInfo> all = productRepo.findAll(pageRequest);

        pageVO.setT(all.getContent());
        pageVO.setTotalPage(all.getTotalPages());
        pageVO.setTotalCount(all.getTotalElements());

        return pageVO;
    }

    //针对后台管理界面需要分页
    public PageVO<List<ProductInfo>> findProductByPageAndCategoryType(PageDTO<ProductInfo> page) {
        PageRequest pageRequest = PageRequest.of(page.getPageNum(), page.getPageSize());
        PageVO<List<ProductInfo>> pageVO = new PageVO<>();
        ProductInfo productInfo = new ProductInfo();
        productInfo.setCategoryType(page.getT().getCategoryType());
        ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("categoryType", ExampleMatcher.GenericPropertyMatchers.exact());
        Example<ProductInfo> of = Example.of(productInfo, matcher);
        Page<ProductInfo> all = productRepo.findAll(of, pageRequest);
        pageVO.setT(all.getContent());
        pageVO.setTotalPage(all.getTotalPages());
        pageVO.setTotalCount(all.getTotalElements());

        return pageVO;
    }

    //针对微信端 不需要分页
    public List<ProductInfo> findProductByCategoryType(Integer categoryType) {
        PageVO<List<ProductInfo>> pageVO = new PageVO<>();
        ProductInfo productInfo = new ProductInfo();
        productInfo.setCategoryType(categoryType);
        ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("categoryType", ExampleMatcher.GenericPropertyMatchers.exact());
        Example<ProductInfo> of = Example.of(productInfo, matcher);
        List<ProductInfo> all = productRepo.findAll(of);
        return all;
    }

    public List<Map<String ,Object>> showProductList(){
        List<Map<String, Object>> maps = showListRepo.productShowList();
        return maps;
    }
}
