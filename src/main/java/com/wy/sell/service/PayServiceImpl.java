package com.wy.sell.service;

import com.alibaba.fastjson.JSONObject;
import com.wy.sell.config.PayJSConfig;
import com.wy.sell.dto.OrderDTO;
import com.wy.sell.entity.OrderMaster;
import com.wy.sell.repo.OrderMasterRepo;
import com.wy.sell.util.HttpInvoker;
import com.wy.sell.util.SignUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class PayServiceImpl {


    @Autowired
    private PayJSConfig payJSConfig;

    @Autowired
    OrderMasterRepo orderMasterRepo;

    public Map<String,String> payOrder(OrderDTO orderDTO,String returnUrl){
        String notify_url = payJSConfig.getNotifyUrl()+orderDTO.getOrderId();//请注意，，该路径需要payjs服务器可以直接访问，且http状态码为200。测试地址不行，www.baidu.com也不行
        String callback_url = returnUrl;
        Map<String,String> map  = new HashMap<>();

        map.put("mchid",  payJSConfig.getMchid());

        map.put("total_fee",""+(int)(orderDTO.getOrderAmount().doubleValue()*100));
        map.put("out_trade_no",orderDTO.getOrderId());
        map.put("body","");
        map.put("notify_url", notify_url);
        map.put("callback_url", callback_url);
        String md5 = SignUtil.sign(map, payJSConfig.getKey());
        String sign = md5.toUpperCase();
        map.put("sign",sign);
        OrderMaster orderMaster = orderMasterRepo.getOne(orderDTO.getOrderId());
        orderMasterRepo.save(orderMaster);
        return map;
    }
}
