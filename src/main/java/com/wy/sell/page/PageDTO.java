package com.wy.sell.page;

import com.wy.sell.entity.ProductInfo;

public class PageDTO<T> {

    private T t;

    private Integer pageNum;

    private Integer pageSize;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}
