<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link href="https://cdn.bootcss.com/twitter-bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table">
                <thead>
                <tr>
                    <th>
                        订单ID
                    </th>
                    <th>
                        买家姓名
                    </th>
                    <th>
                        买家电话
                    </th>
                    <th>
                        订单创建时间
                    </th>
                    <th>
                        订单状态
                    </th>
                    <th>
                        支付状态
                    </th>
                    <th>
                        金额
                    </th>
                    <th>
                        操作
                    </th>
                </tr>
                </thead>
                <tbody>
                <#list orderDTOPage.getContent() as order>
                <tr>
                    <td>
                       ${order.getOrderId()}
                    </td>
                    <td>
                        ${order.getBuyerName()}
                    </td>
                    <td>
                        ${order.getBuyerPhone()}
                    </td>
                    <td>
                        ${order.getCreateTime()}
                    </td>
                    <td>
                        ${order.getOrderStatus()}
                    </td>
                    <td>
                        ${order.getPayStatus()}
                    </td>
                    <td>
                        ${order.getOrderAmount()}
                    </td>
                    <td>
                        详情
                    </td>
                    <td>
                        取消
                    </td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 column">
        <ul class="pagination pull-right">
            <li>
                <a href="#">上一页</a>
            </li>
            <#list 1..orderDTOPage.getTotalPages() as index>
            <#if currentPage==index >
                <li class="disabled">
                    <a href="#">${index}</a>
                </li>
            <#else >
                <li>
                    <a href="#">${index}</a>
                </li>
            </#if>

            </#list>
            <li>
                <a href="#">下一页</a>
            </li>
        </ul>
    </div>
</div>
</body>
</html>